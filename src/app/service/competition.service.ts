import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Competition {
  id?: string;
  nomCompetition: string;
  nomSaison: string;
}

@Injectable({
  providedIn: 'root'
})
export class CompetitionService {
  private competitionCollection: AngularFirestoreCollection<Competition>;
  private competition: Observable<Competition[]>;
  constructor(public db: AngularFirestore) { 
    this.competitionCollection = db.collection<Competition>('Competitions', ref => ref.orderBy('nomCompetition'));
  }
  addcompetition(competition: Competition) {
    return this.competitionCollection.add(competition);
  }
  get_competition() {
    this.competition = this.competitionCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
    return this.competition;    
  }
  removeCompetition(id) {
    return this.competitionCollection.doc(id).delete();
  }
  get_competition_by_id(id) {
    return this.competitionCollection.doc<Competition>(id).valueChanges();
  }
  updateCompetition(Competition: Competition, id: string) {
    return this.competitionCollection.doc(id).update(Competition);
  }
}
