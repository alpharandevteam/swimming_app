import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Club {
  id?: string;
  nom: string;
  region: string;
  coach: string;
  date_creation: number;
  athletes: [];
}

@Injectable({
  providedIn: 'root'
})
export class ClubServiceService {
  private clubCollection: AngularFirestoreCollection<Club>;
  private club: Observable<Club[]>;
  constructor(public db: AngularFirestore) {
    this.clubCollection = db.collection<Club>('Clubs', ref => ref.orderBy('date_creation'));
  }
  getClub() {
    let list = this.db.collection<Club>('Clubs', ref => ref.orderBy('date_creation'));
    let club =list.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
    return club;
  }
  get_club_by_id(id) {
    return this.clubCollection.doc<Club>(id).valueChanges();
  }
  adduser(users: Club) {
   return this.clubCollection.add(users);
 }
 updateClub(club: Club, id: string) {
  return this.clubCollection.doc(id).update(club);
}
removeClub(id) {
  return this.clubCollection.doc(id).delete();
}
}
