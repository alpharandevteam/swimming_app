import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Athletes {
  id?: string;
  Nom: string;
  Poids: string;
  Age: string;
  Taille: string;
  IdClub: string;
  IdTypeNatation: string;
}
@Injectable({
  providedIn: 'root'
})
export class AthletesService {

  private athletesCollection: AngularFirestoreCollection<Athletes>;

  private athletes: Observable<Athletes[]>;
  constructor(public db: AngularFirestore) {
    this.athletesCollection = db.collection<Athletes>('athletes', ref => ref.orderBy('Nom'));
   }
   Getathletes(id) {
    return this.athletesCollection.doc<Athletes>(id).valueChanges();
  }

  Getathlete() {
    this.athletes = this.athletesCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
    return this.athletes;
  }

  updateathletes(saison: Athletes, id: string) {
    return this.athletesCollection.doc(id).update(saison);
  }

  addathletes(saison: Athletes) {
    console.log("service info athlete ="+saison);
    return this.athletesCollection.add(saison);
  }

  removeathletes(id) {
    return this.athletesCollection.doc(id).delete();
  }
  Getathletes_by_Club(IdClub) {
   this.athletesCollection = this.db.collection<Athletes>('athletes', ref => ref.where('IdClub', '==', IdClub));
   let athletest = this.athletesCollection.snapshotChanges().pipe(
     map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
    console.log("get by id athlete = "+athletest);
    return athletest;
  }

  updade_membre_club(dataAthlete) {
    return this.athletesCollection.doc(dataAthlete.id).update(dataAthlete);
  }
  getbyid(nom){
    let req= this.db.collection<Athletes>('athletes', ref => ref.where('IdTypeNatation', '==',nom));
     let list = req.snapshotChanges().pipe(
       map(actions => {
         return actions.map(a => {
           const data = a.payload.doc.data();
           const id = a.payload.doc.id;
           return { id, ...data };
         });
       })
     );
     return list;
   }
}
