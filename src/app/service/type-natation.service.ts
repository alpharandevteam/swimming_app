import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface TypeNatation {
  id?: string;
  NomType: string;
}

@Injectable({
  providedIn: 'root'
})
export class TypeNatationService {
  private typenatationCollection: AngularFirestoreCollection<TypeNatation>;

  private typenatation: Observable<TypeNatation[]>;
  constructor(public db: AngularFirestore) {
    this.typenatationCollection = db.collection<TypeNatation>('typenatation', ref => ref.orderBy('NomType'));
   }
   GetTypeNatation(id) {
    return this.typenatationCollection.doc<TypeNatation>(id).valueChanges();
  }

  GetTypeNatations() {
    let ttt= this.db.collection<TypeNatation>('typenatation', ref => ref.orderBy('NomType'));
    let typenatation = ttt.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
    return typenatation;
  }

  updateTypeNatation(typenatation: TypeNatation, id: string) {
    return this.typenatationCollection.doc(id).update(typenatation);
  }

  addTypeNatation(typenatation: TypeNatation) {
    return this.typenatationCollection.add(typenatation);
  }

  removeTypeNatation(id) {
    return this.typenatationCollection.doc(id).delete();
  }
}
