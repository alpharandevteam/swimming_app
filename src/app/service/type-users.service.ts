import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Type_Users {
  id?: string;
  NomType: string;
}

@Injectable({
  providedIn: 'root'
})
export class TypeUsersService {

  private type_usersCollection: AngularFirestoreCollection<Type_Users>;

  private type_users: Observable<Type_Users[]>;

  constructor(db: AngularFirestore) {
    this.type_usersCollection = db.collection<Type_Users>('type_users', ref => ref.orderBy('NomType'));
  }

  GetType_Users(id) {
    return this.type_usersCollection.doc<Type_Users>(id).valueChanges();
  }

  GetType_User() {
    this.type_users = this.type_usersCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
    return this.type_users;
  }

  updateType_User(type_users: Type_Users, id: string) {
    return this.type_usersCollection.doc(id).update(type_users);
  }

  addType_User(type_users: Type_Users) {
    return this.type_usersCollection.add(type_users);
  }

  removeType_User(id) {
    return this.type_usersCollection.doc(id).delete();
  }

}
