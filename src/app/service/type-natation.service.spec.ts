import { TestBed } from '@angular/core/testing';

import { TypeNatationService } from './type-natation.service';

describe('TypeNatationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TypeNatationService = TestBed.get(TypeNatationService);
    expect(service).toBeTruthy();
  });
});
