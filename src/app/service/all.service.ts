import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
export interface Athletes {
  id?: string;
  Nom: string;
  Poids: string;
  Age: string;
  Taille: string;
  IdClub: string;
  IdTypeNatation: string;
}
export interface User {
  id?: string;
  login: string;
  password: string;
  mail: string;
  typeUser: string;
  gestionForgotpwd: string;
  createdAt: number;
  club: string;
}
export interface Club {
  id?: string;
  nom: string;
  region: string;
  coach: string;
  date_creation: number;
  athletes: [];
}
export interface Competition {
  id?: string;
  nomCompetition: string;
  nomSaison: string;
}
export interface Performance {
  id?: string;
  Chronos: string;
  IdAthletes: string;
  IdTypeNatation: string;
}
export interface Saison {
  id?: string;
  Nom: string;
  Date_Debut: string;
  Date_Fin: string;
}
export interface TypeNatation {
  id?: string;
  NomType: string;
}
export interface Type_Users {
  id?: string;
  NomType: string;
}
@Injectable({
  providedIn: 'root'
})
export class AllService {
  private athletesCollection: AngularFirestoreCollection<Athletes>;
  private userCollection: AngularFirestoreCollection<User>;
  private clubCollection: AngularFirestoreCollection<Club>;
  private competitionCollection: AngularFirestoreCollection<Competition>;
  private users: Observable<User[]>;
  private performanceCollection: AngularFirestoreCollection<Performance>;
  private saisonCollection: AngularFirestoreCollection<Saison>;
  private typenatationCollection: AngularFirestoreCollection<TypeNatation>;
  private type_usersCollection: AngularFirestoreCollection<Type_Users>;
  constructor(public db: AngularFirestore) {
    this.athletesCollection = db.collection<Athletes>('athletes', ref => ref.orderBy('Nom'));
    this.userCollection = db.collection<User>('users', ref => ref.orderBy('createdAt'));
    this.clubCollection = db.collection<Club>('Clubs', ref => ref.orderBy('date_creation'));
    this.competitionCollection = db.collection<Competition>('Competitions', ref => ref.orderBy('nomCompetition'));
    this.performanceCollection = db.collection<Performance>('Performances', ref => ref.orderBy('Chronos'));
    this.saisonCollection = db.collection<Saison>('saison', ref => ref.orderBy('Date_Debut'));
    this.typenatationCollection = db.collection<TypeNatation>('typenatation', ref => ref.orderBy('NomType'));
    this.type_usersCollection = db.collection<Type_Users>('type_users', ref => ref.orderBy('NomType'));
   }
   test(){
    this.athletesCollection;
    this.userCollection;
    this.clubCollection;
    this.competitionCollection;
    this.performanceCollection;
    this.saisonCollection;
    this.typenatationCollection;
    this.type_usersCollection;
    console.log('loaded');
   }
}
