import { Injectable } from '@angular/core';
import * as bcrypt from 'bcryptjs';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface User {
  id?: string;
  login: string;
  password: string;
  mail: string;
  typeUser: string;
  gestionForgotpwd: string;
  createdAt: number;
  club: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthenticationServiceService {

  private userCollection: AngularFirestoreCollection<User>;
  private users: Observable<User[]>;

  constructor(public db: AngularFirestore) {
    this.userCollection = db.collection<User>('users', ref => ref.orderBy('createdAt'));
   }

   getUsers() {
    this.users = this.userCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
    return this.users;
  }

   adduser(users: User, pass) {
     users.password = pass;
     return this.userCollection.add(users);
   }
  getUser(idUser) {
    // return this.userCollection.doc<User>(idUser).valueChanges();
    this.userCollection = this.db.collection<User>('users', ref => ref.where('id', '==', idUser));
    this.users = this.userCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
    return this.users;
  }
  get_user_by_id(iduser) {
    return this.userCollection.doc<User>(iduser).valueChanges();
  }

  updateUser(users: User, id: string) {
    return this.userCollection.doc(id).update(users);
  }
  removeUser(id) {
    return this.userCollection.doc(id).delete();
  }
  get_user_by_login(login) {
    const get = this.db.collection<User>('users', ref => ref.where('login', '==', login));
    const res = get.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
    return res;
  }
  }
