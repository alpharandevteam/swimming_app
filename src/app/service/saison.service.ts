import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Saison {
  id?: string;
  Nom: string;
  Date_Debut: string;
  Date_Fin: string;
}
@Injectable({
  providedIn: 'root'
})
export class SaisonService {
  private saisonCollection: AngularFirestoreCollection<Saison>;

  private saison: Observable<Saison[]>;
  constructor(db: AngularFirestore) {
    this.saisonCollection = db.collection<Saison>('saison', ref => ref.orderBy('Date_Debut'));
   }
   Getsaison(id) {
    return this.saisonCollection.doc<Saison>(id).valueChanges();
  }

  Getsaisons() {
    this.saison = this.saisonCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
    return this.saison;
  }

  updatesaison(saison: Saison, id: string) {
    return this.saisonCollection.doc(id).update(saison);
  }

  addsaison(saison: Saison) {
    return this.saisonCollection.add(saison);
  }

  removesaison(id) {
    return this.saisonCollection.doc(id).delete();
  }
}
