import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Performance {
  id?: string;
  Chronos: string;
  IdAthletes: string;
  IdTypeNatation: string;
  typecompet: string;
  Date: number;
}

@Injectable({
  providedIn: 'root'
})
export class PerformanceService {
  private performanceCollection: AngularFirestoreCollection<Performance>;
  private performance: Observable<Performance[]>;
  constructor(public db: AngularFirestore) { 
    this.performanceCollection = db.collection<Performance>('Performances', ref => ref.orderBy('Date'));
  }
  addperformance(performance: Performance) {
    return this.performanceCollection.add(performance);
  }
  get_performance() {
    this.performance = this.performanceCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
    return this.performance;    
  }
  removeperformance(id) {
    return this.performanceCollection.doc(id).delete();
  }
  get_performance_by_id(id) {
    return this.performanceCollection.doc<Performance>(id).valueChanges();
  }
  updateperformance(performance: Performance, id: string) {
    return this.performanceCollection.doc(id).update(performance);
  }
  getbynom(nom){
    let req= this.db.collection<Performance>('Performances', ref => ref.where('IdAthletes', '==',nom));
     let list = req.snapshotChanges().pipe(
       map(actions => {
         return actions.map(a => {
           const data = a.payload.doc.data();
           const id = a.payload.doc.id;
           return { id, ...data };
         });
       })
     );
     return list;
   }
   getbynomcompet(nom){
    let req= this.db.collection<Performance>('Performances', ref => ref.where('typecompet', '==',nom));
     let list = req.snapshotChanges().pipe(
       map(actions => {
         return actions.map(a => {
           const data = a.payload.doc.data();
           const id = a.payload.doc.id;
           return { id, ...data };
         });
       })
     );
     return list;
   }
}
