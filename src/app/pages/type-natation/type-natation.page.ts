import { Component, OnInit } from '@angular/core';
import { TypeNatation, TypeNatationService } from './../../service/type-natation.service';
import { NavController, LoadingController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-type-natation',
  templateUrl: './type-natation.page.html',
  styleUrls: ['./type-natation.page.scss'],
})
export class TypeNatationPage implements OnInit {
  typenatation: TypeNatation = {
    NomType: ''
  };
  typenatationId = null;
  resultat = null;
  constructor(private route: ActivatedRoute, private nav: NavController,private typenatationservice: TypeNatationService, private loadingController: LoadingController) { }

  ngOnInit() {
    this.typenatationId = this.route.snapshot.params['id'];
      this.resultat = JSON.parse(this.typenatationId);
  }
  // async loadTypeNatation() {
  //   const loading = await this.loadingController.create({
  //     message: 'Loading type utilisateur..'
  //   });
  //   await loading.present();

  //   this.typenatationservice.GetTypeNatation(this.typenatationId).subscribe(res => {
  //     loading.dismiss();
  //     this.typenatation = res;
  //   });
  // }
  async updatsTypeNatation(type_natationId,type_natationn) {
    //alert(type_usersId);
      console.log(type_natationId);

    const loading = await this.loadingController.create({
      message: 'Modification en cours..'
    });
      await loading.present();
       this.typenatation.NomType=type_natationn;
       //this.type_user.id=type_usersId;
       console.log(this.typenatation);
       this.typenatationservice.updateTypeNatation(this.typenatation,type_natationId).then(() => {
         loading.dismiss();
         this.nav.navigateForward('list-type-natation');
     });
  }
  async saveTypeNatation() {

    const loading = await this.loadingController.create({
      message: 'Enregistrement en cours..'
    });
    await loading.present();
      this.typenatationservice.addTypeNatation(this.typenatation).then(() => {
        loading.dismiss();
        this.nav.navigateForward('list-type-natation');
      });
  }
}
