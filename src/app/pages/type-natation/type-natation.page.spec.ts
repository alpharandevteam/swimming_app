import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeNatationPage } from './type-natation.page';

describe('TypeNatationPage', () => {
  let component: TypeNatationPage;
  let fixture: ComponentFixture<TypeNatationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypeNatationPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeNatationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
