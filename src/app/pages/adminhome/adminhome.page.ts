import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController } from '@ionic/angular';
@Component({
  selector: 'app-adminhome',
  templateUrl: './adminhome.page.html',
  styleUrls: ['./adminhome.page.scss'],
})
export class AdminhomePage implements OnInit {

  constructor(private nav: NavController) { }

  ngOnInit() {
  }
  gestclub(){
    this.nav.navigateForward('club');
  }
  gestuser(){
    // this.nav.navigateForward('users/add');
    alert("liste utilisateur");
    this.nav.navigateForward('gestion-utilisateurs');
  }
  gesttypeuser(){
    this.nav.navigateForward('list-type-user');   
  }
  gestsaison(){
    this.nav.navigateForward('list-saison');
  }
  gesttypenatation(){
    this.nav.navigateForward('list-type-natation');
  }
  gestathletes(){
    this.nav.navigateForward('list-athletes');
  }
  gestcompetition(){
    this.nav.navigateForward('gestion-competition');
  }
  gestlogin(){
    this.nav.navigateForward('login');
  }
}
