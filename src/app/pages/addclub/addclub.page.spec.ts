import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddclubPage } from './addclub.page';

describe('AddclubPage', () => {
  let component: AddclubPage;
  let fixture: ComponentFixture<AddclubPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddclubPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddclubPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
