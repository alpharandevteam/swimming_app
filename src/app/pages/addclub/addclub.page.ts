import { Component, OnInit } from '@angular/core';
import { Club, ClubServiceService } from '../../service/club-service.service';
import { Athletes, AthletesService } from './../../service/athletes.service';
import { ActivatedRoute } from '@angular/router';
import { NavController, LoadingController, ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-addclub',
  templateUrl: './addclub.page.html',
  styleUrls: ['./addclub.page.scss'],
})
export class AddclubPage implements OnInit {
  users: Club = {
    id: '',
    nom: '',
    date_creation: new Date().getTime(),
    region: '',
    coach: '',
    athletes: [],
  };
  checked = 0;
  club: [];
  for_update: Club;
  listAthlete;
  selectedArray: any = [];
  // tslint:disable-next-line:max-line-length
  constructor(private route: ActivatedRoute, private nav: NavController, private clubService: ClubServiceService, private loadingController: LoadingController, private ath: AthletesService) {
    const getted = this.route.snapshot.paramMap.get('categories');
    if (getted !== 'add' ) {
      console.log(JSON.parse(getted));
      this.for_update = JSON.parse(getted);
    }
   }

 async ngOnInit() {
  await this.ath.Getathlete().subscribe(res => {
    console.log(res);
    this.listAthlete = res;
  });
  }
  async Save_club(form) {
    const loading = await this.loadingController.create({
      message: 'loading...'
    });
    await loading.present();
    this.users = form.value;
    this.users.date_creation = new Date().getTime();
      this.clubService.adduser(this.users).then(() => {
        loading.dismiss();
        this.nav.navigateForward('club');
      });
  }
  async  updade_club_data(for_update) {
    const loading = await this.loadingController.create({
      message: 'loading...'
    });
    await loading.present();
    for_update.date_creation = new Date().getTime();
    this.clubService.updateClub(for_update, for_update.id).then(() => {
      loading.dismiss();
      this.nav.navigateForward('club');
    });
  }
  addMember() {
    if (this.checked === 0) {
      this.checked = 1;
    }
    // if (this.checked === 1) {
    //   this.checked = 0;
    // }
  }
  addCheckbox(event, iduser) {
    if (event.detail.checked) {
      this.selectedArray.push(iduser);
    } else {
      const index = this.removeCheckedFromArray(iduser);
      this.selectedArray.splice(index, 1);
    }
    console.log(this.selectedArray);
  }
  removeCheckedFromArray(checkbox) {
    this.selectedArray.findIndex((category) => {
   });
 }
 async insertathfromclub(for_update) {
  const loading = await this.loadingController.create({
    message: 'loading...'
  });
  await loading.present();
  for_update.date_creation = new Date().getTime();
  for_update.Athletes = this.selectedArray;
  this.clubService.updateClub(for_update, for_update.id).then(() => {
    loading.dismiss();
    this.nav.navigateForward('club');
  });
 }
}
