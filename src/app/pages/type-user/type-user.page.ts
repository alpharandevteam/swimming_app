import { Component, OnInit } from '@angular/core';
import { Type_Users, TypeUsersService } from './../../service/type-users.service';
import { NavController, LoadingController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-type-user',
  templateUrl: './type-user.page.html',
  styleUrls: ['./type-user.page.scss'],
})
export class TypeUserPage implements OnInit {
  type_users: Type_Users[];
  type_user: Type_Users = {
    NomType: '',
  };
  type_usersId = null;
  resultat = null;
  constructor(private route: ActivatedRoute, private nav: NavController,private typeusersservice: TypeUsersService, private loadingController: LoadingController) {
    // this.typeusersservice.GetType_User().subscribe(res => {
    //   this.type_users = res;
    // });
   }

  ngOnInit() {
    this.type_usersId = this.route.snapshot.params['id'];
      this.resultat = JSON.parse(this.type_usersId);
      console.log(this.resultat);
  }
  async loadType_Users() {
    const loading = await this.loadingController.create({
      message: 'Loading type utilisateur..'
    });
    await loading.present();

    this.typeusersservice.GetType_Users(this.type_usersId).subscribe(res => {
      loading.dismiss();
      this.type_user = res;
    });
  }
  async updatsType_User(type_userId,type_useree) {
    //alert(type_usersId);
    console.log(type_userId);

    const loading = await this.loadingController.create({
      message: 'Modification en cours..'
    });
      await loading.present();
       this.type_user.NomType=type_useree;
       //this.type_user.id=type_usersId;
       console.log(this.type_user);
       this.typeusersservice.updateType_User(this.type_user,type_userId).then(() => {
         loading.dismiss();
         this.nav.navigateForward('list-type-user');
     });
  }
  async saveType_User() {
    const loading = await this.loadingController.create({
      message: 'Enregistrement en cours..'
    });
    await loading.present();
    console.log(this.type_user);
      this.typeusersservice.addType_User(this.type_user).then(() => {
        loading.dismiss();
        this.nav.navigateForward('list-type-user');
      });
  }
}
