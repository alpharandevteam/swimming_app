import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListSaisonPage } from './list-saison.page';

describe('ListSaisonPage', () => {
  let component: ListSaisonPage;
  let fixture: ComponentFixture<ListSaisonPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListSaisonPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListSaisonPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
