import { Component, OnInit } from '@angular/core';
import { Saison, SaisonService } from './../../service/saison.service';
import { NavController, LoadingController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-list-saison',
  templateUrl: './list-saison.page.html',
  styleUrls: ['./list-saison.page.scss'],
})
export class ListSaisonPage implements OnInit {
  saisons: Saison[];
  saison: Saison = {
    Nom: '',
    Date_Debut: '',
    Date_Fin:''
  };
  saisonsId = null;
  constructor(private route: ActivatedRoute, private nav: NavController,private saisonservice: SaisonService, private loadingController: LoadingController) {
    
   }

  async ngOnInit() {
    await this.saisonservice.Getsaisons().subscribe(res => {
      this.saisons = res;
      console.log(this.saisons);
    });
  }
  async goto_insert_saison(){
    let j=null;
    this.nav.navigateForward('saison/' +j);
  }
  async goto_modifie_saison(id){
    const loading = await this.loadingController.create({
      message: 'Loading type utilisateur..'
    });
    await loading.present();
    this.saisonservice.Getsaison(id).subscribe(res => {
      loading.dismiss();
      res.id = id;
       console.log('frfdfd' + res);
    console.log(JSON.stringify(res));
    this.nav.navigateForward('saison/' + JSON.stringify(res));
    });
  }
  async delete(saisonId){
    const loading = await this.loadingController.create({
      message: 'Suppression en cours..',
      duration: 5000
    });
    await loading.present();
      this.saisonservice.removesaison(saisonId).then(() => {
      loading.dismiss();
      this.nav.navigateForward('list-saison');
  });
  }
}
