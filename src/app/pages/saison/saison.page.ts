import { Component, OnInit } from '@angular/core';
import { Saison, SaisonService } from './../../service/saison.service';
import { NavController, LoadingController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-saison',
  templateUrl: './saison.page.html',
  styleUrls: ['./saison.page.scss'],
})
export class SaisonPage implements OnInit {
  saison: Saison = {
    Nom: '',
    Date_Debut: '',
    Date_Fin:''
  };
  saisonId = null;
  resultat = null;
  constructor(private route: ActivatedRoute, private nav: NavController,private saisonservice: SaisonService, private loadingController: LoadingController) { }

  ngOnInit() {
    this.saisonId = this.route.snapshot.params['id'];
    this.resultat = JSON.parse(this.saisonId);
  }
  // async loadsaison() {
  //   const loading = await this.loadingController.create({
  //     message: 'Loading type utilisateur..'
  //   });
  //   await loading.present();

  //   this.saisonservice.Getsaison(this.saisonId).subscribe(res => {
  //     loading.dismiss();
  //     this.saison = res;
  //     console.log(this.saison);
  //   });
  // }
  async updatssaison(saisonId,saisonom,saisodebut,saisonfin) {
    //alert(type_usersId);
      console.log(saisonId);

    const loading = await this.loadingController.create({
      message: 'Modification en cours..'
    });
      await loading.present();
       this.saison.Nom=saisonom;
       this.saison.Date_Debut=saisodebut;
       this.saison.Date_Fin=saisonfin;
       //this.type_user.id=type_usersId;
       console.log(this.saisonservice);
       this.saisonservice.updatesaison(this.saison,saisonId).then(() => {
         loading.dismiss();
         this.nav.navigateForward('list-saison');
     });
  }
  async savesaison() {
    const loading = await this.loadingController.create({
      message: 'Enregistrement en cours..'
    });
    await loading.present();
      this.saisonservice.addsaison(this.saison).then(() => {
        loading.dismiss();
        this.nav.navigateForward('list-saison');
      });
  }
}
