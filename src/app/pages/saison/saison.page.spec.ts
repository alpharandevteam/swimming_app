import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaisonPage } from './saison.page';

describe('SaisonPage', () => {
  let component: SaisonPage;
  let fixture: ComponentFixture<SaisonPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaisonPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaisonPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
