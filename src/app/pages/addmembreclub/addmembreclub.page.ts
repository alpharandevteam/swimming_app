import { Component, OnInit } from '@angular/core';
import { Athletes, AthletesService } from './../../service/athletes.service';
import { Club, ClubServiceService } from '../../service/club-service.service';
import { TypeNatation, TypeNatationService } from './../../service/type-natation.service';
import { NavController, LoadingController } from '@ionic/angular';
import { ActivatedRoute,Router  } from '@angular/router';

@Component({
  selector: 'app-addmembreclub',
  templateUrl: './addmembreclub.page.html',
  styleUrls: ['./addmembreclub.page.scss'],
})
export class AddmembreclubPage implements OnInit {
  athletes: Athletes = {
    Nom: '',
    Poids: '',
    Age: '',
    Taille: '',
    IdClub: '',
    IdTypeNatation: ''
  };
  listtypenatation;
  coachClub;
  modifathlete = null;
  dataAthlete = [];
  nbrRes;
  idcoachClub;
  constructor(private route: ActivatedRoute,private router: Router, private nav: NavController, private athletesservice: AthletesService, private loadingController: LoadingController, private typenatationservice: TypeNatationService, private clubservice: ClubServiceService) {
    this.typenatationservice.GetTypeNatations().subscribe( res => {
      this.listtypenatation = res;
    });
  }

  async ngOnInit() {
    const clubCoach = this.route.snapshot.paramMap.get('coachClub');
    
    // this.coachClub = clubCoach.length;
    // alert(this.coachClub);

    this.nbrRes = clubCoach.length;
    //alert(this.coachClub);
    if(this.nbrRes<20){
      this.coachClub = clubCoach;
      //alert("nbr res inf 20 = "+this.coachClub);
    }else{
      this.idcoachClub = JSON.parse(clubCoach);
      this.coachClub = this.idcoachClub.IdClub;
      //alert("nbr res sup 20 = "+this.coachClub);
    }

    console.log(this.coachClub);
    if (JSON.parse(clubCoach)) {
      this.modifathlete = "update_athlete";
      this.dataAthlete = JSON.parse(clubCoach);
    } else {
      console.log("nom club =" + clubCoach);
      this.coachClub = clubCoach;      
    }
  }

  async Save_athlete(form) {
    const loading = await this.loadingController.create({
      message: 'loading...'
    });
    await loading.present();
    this.athletes = form.value;
    this.athletes.IdClub = this.coachClub;
    console.log("info athelere =" + JSON.stringify(this.athletes));
    this.athletesservice.addathletes(this.athletes).then(() => {
      loading.dismiss();
      this.nav.navigateRoot('listemembreclub/' + this.coachClub);
    });
  }

  async  updade_membre_club(dataAthlete) {
    console.log("update data membre");
    const loading = await this.loadingController.create({
      message: 'loading...'
    });
    await loading.present();
    this.athletesservice.updade_membre_club(dataAthlete).then(() => {
      loading.dismiss();
      this.nav.navigateRoot ('/listemembreclub/' + dataAthlete.IdClub);
    });
  }

}
