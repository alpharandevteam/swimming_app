import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddmembreclubPage } from './addmembreclub.page';

describe('AddmembreclubPage', () => {
  let component: AddmembreclubPage;
  let fixture: ComponentFixture<AddmembreclubPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddmembreclubPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddmembreclubPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
