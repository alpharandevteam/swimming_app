import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListTypeNatationPage } from './list-type-natation.page';

describe('ListTypeNatationPage', () => {
  let component: ListTypeNatationPage;
  let fixture: ComponentFixture<ListTypeNatationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListTypeNatationPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListTypeNatationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
