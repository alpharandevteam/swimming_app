import { Component, OnInit } from '@angular/core';
import { TypeNatation, TypeNatationService  } from './../../service/type-natation.service';
import { NavController, LoadingController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-list-type-natation',
  templateUrl: './list-type-natation.page.html',
  styleUrls: ['./list-type-natation.page.scss'],
})
export class ListTypeNatationPage implements OnInit {
  typenatations: TypeNatation[];
  typenatation: TypeNatation = {
    NomType: ''
  };
  typenatationId = null;
  resultat = null;
  constructor(private route: ActivatedRoute, private nav: NavController,private typenatationservice: TypeNatationService, private loadingController: LoadingController) {
    this.typenatationservice.GetTypeNatations().subscribe(res => {
      this.typenatations = res;
    });
   }

  ngOnInit() {
  }
  async goto_insert_type_natation(){
    let j=null;
    this.nav.navigateForward('type-natation/' +j);
  }
  async goto_modifie_type_natation(id){
    const loading = await this.loadingController.create({
      message: 'Loading type utilisateur..'
    });
    await loading.present();
    this.typenatationservice.GetTypeNatation(id).subscribe(res => {
      loading.dismiss();
      res.id = id;
       console.log('frfdfd' + res);
    console.log(JSON.stringify(res));
    this.nav.navigateForward('type-natation/' + JSON.stringify(res));
    });
  }
  async delete(type_natationId){
    const loading = await this.loadingController.create({
      message: 'Suppression en cours..',
      duration: 5000
    });
    await loading.present();
      this.typenatationservice.removeTypeNatation(type_natationId).then(() => {
      loading.dismiss();
      this.nav.navigateForward('list-type-natation');
  });
  }
}
