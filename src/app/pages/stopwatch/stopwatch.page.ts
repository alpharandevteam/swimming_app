import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, LoadingController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { Performance, PerformanceService } from './../../service/performance.service';
@Component({
  selector: 'app-stopwatch',
  templateUrl: './stopwatch.page.html',
  styleUrls: ['./stopwatch.page.scss'],
})
export class StopwatchPage implements OnInit {
  performance: Performance ={
    Chronos: '',
    IdAthletes: '',
    IdTypeNatation: '',
    typecompet:'',
    Date:new Date().getTime(),
  }
  public timeBegan = null;
  public timeStopped: any = null;
  public stoppedDuration: any = 0;
  public started = null;
  public running = false;
  public blankTime = '00:00.000';
  public time = '00:00.000';
  public captured1 = '';
  public captured2 = '';
  public captured3 = '';
  public captured4 = '';
  public captured5 = '';
  public captured6 = '';
  public captured7 = '';
  public captured8 = '';
  public captured9 = '';
  public captured10 = '';
  public captured11 = '';
  public captured12 = '';
  public captured13 = '';
  public captured14 = '';
  public captured15 = '';
  public captured16 = '';
  nb = 0;
  max = 0;
  athlete = null;
  resultat = null;
  club: any;
  typenatation = null;
  typecompet = null;
  constructor(private nav: NavController, private loadingController: LoadingController, private route:ActivatedRoute,private performanceCollection:PerformanceService) {
    this.athlete = this.route.snapshot.params['item'];
      this.resultat = JSON.parse(this.athlete);
      console.log(this.resultat);
      this.max = this.resultat.length;
       console.log(this.max);
   }

  ngOnInit() {
    this.club = this.route.snapshot.params['id'];
    this.typenatation = this.route.snapshot.params['IdTypeNatation'];
    this.typecompet = this.route.snapshot.params['idcompet'];
    console.log(this.typenatation);
  }
  start() {
    if (this.running) return;
    if (this.timeBegan === null) {
        this.reset();
        this.timeBegan = new Date();
    }
    if (this.timeStopped !== null) {
      let newStoppedDuration : any = (+new Date() - this.timeStopped);
      this.stoppedDuration = this.stoppedDuration + newStoppedDuration;
    }
    this.started = setInterval(this.clockRunning.bind(this), 10);
      this.running = true;
    }

    stop() {
      this.running = false;
      this.timeStopped = new Date();
      clearInterval(this.started);
   }
capture() {
  console.log(this.max);
  if (this.max > this.nb) {
    this.nb += 1;
  if (this.nb === 1) {
    this.captured1 = this.time;
  }
  if (this.nb === 2) {
    this.captured2 = this.time;
  }
  if (this.nb === 3) {
    this.captured3 = this.time;
  }
  if (this.nb === 4) {
    this.captured4 = this.time;
  }
  if (this.nb === 5) {
    this.captured5 = this.time;
  }
  if (this.nb === 6) {
    this.captured6 = this.time;
  }
  if (this.nb === 7) {
    this.captured7 = this.time;
  }
  if (this.nb === 8) {
    this.captured8 = this.time;
  }
  if (this.nb === 9) {
    this.captured9 = this.time;
  }
  if (this.nb === 10) {
    this.captured10 = this.time;
  }
  if (this.nb === 11) {
    this.captured11 = this.time;
  }
  if (this.nb === 12) {
    this.captured12 = this.time;
  }
  if (this.nb === 13) {
    this.captured13 = this.time;
  }
  if (this.nb === 14) {
    this.captured14 = this.time;
  }
  if (this.nb === 15) {
    this.captured15 = this.time;
  }
  if (this.nb === 16) {
    this.captured16 = this.time;
  }
  if (this.max === this.nb) {
    this.stop();
  }
} else {
  this.stop();
}
}
    reset() {
      this.running = false;
      clearInterval(this.started);
      this.stoppedDuration = 0;
      this.timeBegan = null;
      this.timeStopped = null;
      this.time = this.blankTime;
    }
    zeroPrefix(num, digit) {
      let zero = '';
      for (let i = 0; i < digit; i++) {
        zero += '0';
      }
      return (zero + num).slice(-digit);
    }
  clockRunning() {
    let currentTime: any = new Date();
    let timeElapsed: any = new Date(currentTime - this.timeBegan - this.stoppedDuration);
    let hour = timeElapsed.getUTCHours();
    let min = timeElapsed.getUTCMinutes();
    let sec = timeElapsed.getUTCSeconds();
    let ms = timeElapsed.getUTCMilliseconds();
    this.time =
      this.zeroPrefix(hour, 2) + ':' +
      this.zeroPrefix(min, 2) + ':' +
      this.zeroPrefix(sec, 2) + '.' +
      this.zeroPrefix(ms, 3);
  }
  async valider_chronos(typenatation,typecompet,captured1,oppenent1,captured2,oppenent2,captured3,oppenent3,captured4,oppenent4,captured5,oppenent5,captured6,oppenent6,captured7,oppenent7,captured8,oppenent8,captured9,oppenent9,captured10,oppenent10,captured11,oppenent11,captured12,oppenent12,captured13,oppenent13,captured14,oppenent14,captured15,oppenent15,captured16,oppenent16){
  if(oppenent1 != undefined && oppenent2 != undefined){
    if(captured1 !='' && captured1 != null && captured1 != undefined){
      const loading = await this.loadingController.create({
        message: 'En cours..'      
      });
      await loading.present();
      for(let i = 0; i <= this.max; i++){
        let j = 0;
        let b = 0;
        let table = [];
        let tabn = [];
        let tab = typenatation;
        console.log(tab);
        if(j+i === 1 && b+i===1){
          table[1] = captured1;
          tabn[1] = oppenent1;
          this.performance.Chronos = table[1];
          this.performance.IdAthletes = tabn[1];
          this.performance.IdTypeNatation = tab;
          this.performance.typecompet = typecompet;
          console.log(this.performance);
          this.performanceCollection.addperformance(this.performance).then(() => {
            loading.dismiss();
            this.nav.navigateForward('performance/' + this.club);
          });
        }else if(j+i === 2 && b+i===2){
            table[2] = captured2;
            tabn[2] = oppenent2;
            this.performance.Chronos =table[2];
            this.performance.IdAthletes = tabn[2];
            this.performance.typecompet = tab;
            this.performance.typecompet = typecompet;
            console.log(this.performance);
            this.performanceCollection.addperformance(this.performance).then(() => {
              loading.dismiss();
              this.nav.navigateForward('performance/' + this.club);
            });
          }else if(j+i === 3 && b+i===3){
            table[3] = captured3;
            tabn[3] = oppenent3;
            this.performance.Chronos =table[3];
            this.performance.IdAthletes = tabn[3];
            this.performance.IdTypeNatation = tab;
            this.performance.typecompet = typecompet;
            console.log(this.performance);
            this.performanceCollection.addperformance(this.performance).then(() => {
              loading.dismiss();
              this.nav.navigateForward('performance/' + this.club);
            });
          }else if(j+i === 4 && b+i===4){
            table[4] = captured4;
            tabn[4] = oppenent4;
            this.performance.Chronos =table[4];
            this.performance.IdAthletes = tabn[4];
            this.performance.IdTypeNatation = tab;
            this.performance.typecompet = typecompet;
            console.log(this.performance);
            this.performanceCollection.addperformance(this.performance).then(() => {
              loading.dismiss();
              this.nav.navigateForward('performance/' + this.club);
            });
          }else if(j+i === 5 && b+i===5){
            table[5] = captured5;
            tabn[5] = oppenent5;
            this.performance.Chronos =table[5];
            this.performance.IdAthletes = tabn[5];
            this.performance.IdTypeNatation = tab;
            this.performance.typecompet = typecompet;
            console.log(this.performance);
            this.performanceCollection.addperformance(this.performance).then(() => {
              loading.dismiss();
              this.nav.navigateForward('performance/' + this.club);
            });
          }else if(j+i === 6 && b+i===6){
            table[6] = captured6;
            tabn[6] = oppenent6;
            this.performance.Chronos =table[6];
            this.performance.IdAthletes = tabn[6];
            this.performance.IdTypeNatation = tab;
            this.performance.typecompet = typecompet;
            console.log(this.performance);
            this.performanceCollection.addperformance(this.performance).then(() => {
              loading.dismiss();
              this.nav.navigateForward('performance/' + this.club);
            });
          }else if(j+i === 7 && b+i===7){
            table[7] = captured7;
            tabn[7] = oppenent7;
            this.performance.Chronos =table[7];
            this.performance.IdAthletes = tabn[7];
            this.performance.IdTypeNatation = tab;
            this.performance.typecompet = typecompet;
            console.log(this.performance);
            this.performanceCollection.addperformance(this.performance).then(() => {
              loading.dismiss();
              this.nav.navigateForward('performance/' + this.club);
            });
          }else if(j+i === 8 && b+i===8){
            table[8] = captured8;
            tabn[8] = oppenent8;
            this.performance.Chronos =table[8];
            this.performance.IdAthletes = tabn[8];
            this.performance.IdTypeNatation = tab;
            this.performance.typecompet = typecompet;
            console.log(this.performance);
            this.performanceCollection.addperformance(this.performance).then(() => {
              loading.dismiss();
              this.nav.navigateForward('performance/' + this.club);
            });
          }else if(j+i === 9 && b+i===9){
            table[9] = captured9;
            tabn[9] = oppenent9;
            this.performance.Chronos =table[9];
            this.performance.IdAthletes = tabn[9];
            this.performance.IdTypeNatation = tab;
            this.performance.typecompet = typecompet;
            console.log(this.performance);
            this.performanceCollection.addperformance(this.performance).then(() => {
              loading.dismiss();
              this.nav.navigateForward('performance/' + this.club);
            });
          }else if(j+i === 10 && b+i===10){
            table[10] = captured10;
            tabn[10] = oppenent10;
            this.performance.Chronos =table[10];
            this.performance.IdAthletes = tabn[10];
            this.performance.IdTypeNatation = tab;
            this.performance.typecompet = typecompet;
            console.log(this.performance);
            this.performanceCollection.addperformance(this.performance).then(() => {
              loading.dismiss();
              this.nav.navigateForward('performance/' + this.club);
            });
          }else if(j+i === 11 && b+i===11){
            table[11] = captured11;
            tabn[11] = oppenent11;
            this.performance.Chronos =table[11];
            this.performance.IdAthletes = tabn[11];
            this.performance.IdTypeNatation = tab;
            this.performance.typecompet = typecompet;
            console.log(this.performance);
            this.performanceCollection.addperformance(this.performance).then(() => {
              loading.dismiss();
              this.nav.navigateForward('performance/' + this.club);
            });
          }else if(j+i === 12 && b+i===12){
            table[12] = captured12;
            tabn[12] = oppenent12;
            this.performance.Chronos =table[12];
            this.performance.IdAthletes = tabn[12];
            this.performance.IdTypeNatation = tab;
            this.performance.typecompet = typecompet;
            console.log(this.performance);
            this.performanceCollection.addperformance(this.performance).then(() => {
              loading.dismiss();
              this.nav.navigateForward('performance/' + this.club);
            });
          }else if(j+i === 13 && b+i===13){
            table[13] = captured13;
            tabn[13] = oppenent13;
            this.performance.Chronos =table[13];
            this.performance.IdAthletes = tabn[13];
            this.performance.IdTypeNatation = tab;
            this.performance.typecompet = typecompet;
            console.log(this.performance);
            this.performanceCollection.addperformance(this.performance).then(() => {
              loading.dismiss();
              this.nav.navigateForward('performance/' + this.club);
            });
          }else if(j+i === 14 && b+i===14){
            table[14] = captured14;
            tabn[14] = oppenent14;
            this.performance.Chronos =table[14];
            this.performance.IdAthletes = tabn[14];
            this.performance.IdTypeNatation = tab;
            this.performance.typecompet = typecompet;
            console.log(this.performance);
            this.performanceCollection.addperformance(this.performance).then(() => {
              loading.dismiss();
              this.nav.navigateForward('performance/' + this.club);
            });
          }else if(j+i === 15 && b+i===15){
            table[15] = captured15;
            tabn[15] = oppenent15;
            this.performance.Chronos =table[15];
            this.performance.IdAthletes = tabn[15];
            this.performance.IdTypeNatation = tab;
            this.performance.typecompet = typecompet;
            console.log(this.performance);
            this.performanceCollection.addperformance(this.performance).then(() => {
              loading.dismiss();
              this.nav.navigateForward('performance/' + this.club);
            });
          }else if(j+i === 16 && b+i===16){
            table[16] = captured16;
            tabn[16] = oppenent16;
            this.performance.Chronos =table[16];
            this.performance.IdAthletes = tabn[16];
            this.performance.IdTypeNatation = tab;
            this.performance.typecompet = typecompet;
            console.log(this.performance);
            this.performanceCollection.addperformance(this.performance).then(() => {
              loading.dismiss();
              this.nav.navigateForward('performance/' + this.club);
            });
          }
        console.log(i);
        } 
    }else{
      alert('Veuillez lancer le chrono');
    }
  }else{
    alert("Veuillez remplir le nom des participant");
  }

}
}
