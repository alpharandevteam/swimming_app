import { Component, OnInit } from '@angular/core';
import { Athletes, AthletesService } from './../../service/athletes.service';
import { Club, ClubServiceService } from '../../service/club-service.service';
import { TypeNatation, TypeNatationService } from './../../service/type-natation.service';
import { NavController, LoadingController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-list-athletes',
  templateUrl: './list-athletes.page.html',
  styleUrls: ['./list-athletes.page.scss'],
})
export class ListAthletesPage implements OnInit {
  athlete: Athletes[];
  athletes: Athletes = {
    Nom: '',
    Poids: '',
    Age:'',
    Taille:'',
    IdClub:'',
    IdTypeNatation:''
  };
  listclub = null;
  listtypenatation = null;
  constructor(private route: ActivatedRoute, private nav: NavController,private athletesservice: AthletesService, private loadingController: LoadingController,private typenatationservice:TypeNatationService,private clubservice:ClubServiceService) { }

  async ngOnInit() {
    await this.athletesservice.Getathlete().subscribe(res => {
      this.athlete = res;
      console.log(this.athlete);
    });
    this.listclub = await this.clubservice.getClub().subscribe(res => {
      this.listclub = res;
    });
    this.listtypenatation = await this.typenatationservice.GetTypeNatations().subscribe(res => {
      this.listtypenatation = res;
    });
  }
  async goto_insert_athletes(){
    this.nav.navigateForward('athletes/add');
  }
  async goto_modifie_athletes(id){
      const loading = await this.loadingController.create({
        message: 'Loading type utilisateur..'
      });
      await loading.present();
      this.athletesservice.Getathletes(id).subscribe(res => {
        loading.dismiss();
        res.id = id;
         console.log('parametre');
      console.log(JSON.stringify(res));
      this.nav.navigateForward('athletes/' + JSON.stringify(res));
      });
  }
  async delete(athletesId){
    const loading = await this.loadingController.create({
      message: 'Suppression en cours..',
      duration: 5000
    });
    await loading.present();
      this.athletesservice.removeathletes(athletesId).then(() => {
      loading.dismiss();
      this.nav.navigateForward('list-athletes');
  });
  }
}
