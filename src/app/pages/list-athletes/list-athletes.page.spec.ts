import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAthletesPage } from './list-athletes.page';

describe('ListAthletesPage', () => {
  let component: ListAthletesPage;
  let fixture: ComponentFixture<ListAthletesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListAthletesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAthletesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
