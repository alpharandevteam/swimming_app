import { Component, OnInit } from '@angular/core';
import { Club, ClubServiceService } from '../../service/club-service.service';
import { ActivatedRoute } from '@angular/router';
import { NavController, LoadingController } from '@ionic/angular';
@Component({
  selector: 'app-club',
  templateUrl: './club.page.html',
  styleUrls: ['./club.page.scss'],
})
export class ClubPage implements OnInit {
  club: Club[];
  // tslint:disable-next-line:max-line-length
  constructor(private route: ActivatedRoute, private nav: NavController, private clubService: ClubServiceService, private loadingController: LoadingController) {
   }

   async ngOnInit() {
    const loading = await this.loadingController.create({
      message: 'Chargement..'
    });
    loading.present();
    this.clubService.getClub().subscribe(res => {
      console.log(res);
      this.club = res;
      if ( this.club.length !== 0 &&  this.club.length !== undefined ) {
        loading.dismiss();
      }
    });
  }
  async addClub() {
    this.nav.navigateForward('addclub/add');
  }
  async update_club(categories) {
    const loading = await this.loadingController.create({
      message: 'Loading Todo..'
    });
    await loading.present();
    this.clubService.get_club_by_id(categories).subscribe(res => {
      loading.dismiss();
      res.id = categories;
       console.log('frfdfd' + res);
      console.log(JSON.stringify(res));
       this.nav.navigateForward('addclub/' + JSON.stringify(res));
    });
  }
  async delete_club(id) {
    const loading = await this.loadingController.create({
      message: 'Loading Todo..'
    });
    await loading.present();
    this.clubService.removeClub(id).then(res => {
      loading.dismiss();
       alert('Succes');
    });
  }
}
