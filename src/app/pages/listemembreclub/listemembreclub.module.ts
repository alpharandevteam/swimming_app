import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ListemembreclubPage } from './listemembreclub.page';

const routes: Routes = [
  {
    path: '',
    component: ListemembreclubPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ListemembreclubPage]
})
export class ListemembreclubPageModule {}
