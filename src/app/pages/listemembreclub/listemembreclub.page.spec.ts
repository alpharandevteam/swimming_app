import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListemembreclubPage } from './listemembreclub.page';

describe('ListemembreclubPage', () => {
  let component: ListemembreclubPage;
  let fixture: ComponentFixture<ListemembreclubPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListemembreclubPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListemembreclubPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
