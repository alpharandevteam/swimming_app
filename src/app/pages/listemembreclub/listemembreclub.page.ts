import { Component, OnInit } from '@angular/core';
import { Club, ClubServiceService } from '../../service/club-service.service';
import { Athletes, AthletesService } from './../../service/athletes.service';
import { ActivatedRoute,Router } from '@angular/router';
import { NavController, LoadingController, ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-listemembreclub',
  templateUrl: './listemembreclub.page.html',
  styleUrls: ['./listemembreclub.page.scss'],
})
export class ListemembreclubPage implements OnInit {
  coachClub;
  listeMembre;
  constructor(private route: ActivatedRoute,private router:Router, private nav: NavController, private clubService: ClubServiceService, private loadingController: LoadingController, private ath: AthletesService) {}

  async ngOnInit() {
    const  clubCoach= this.route.snapshot.paramMap.get('club');
    this.coachClub=clubCoach;
    this.listeMembre = await this.ath.Getathletes_by_Club(clubCoach).subscribe(res => {
    this.listeMembre = res;
    });
  }

  ajoutMembre(coachClub){
    //this.nav.navigateForward('athletes/add');
    this.nav.navigateRoot('addmembreclub/'+coachClub);
  }

  async delete_athlete(id) {
    const loading = await this.loadingController.create({
      message: 'Loading ...'
    });
    await loading.present();
    this.ath.removeathletes(id).then(res => {
      loading.dismiss();
       alert('Succes');
    });
  }

  async update_athlete(id) {
    const loading = await this.loadingController.create({
      message: 'Loading ...'
    });
    await loading.present();
    this.ath.Getathletes(id).subscribe(res => {
      loading.dismiss();
      res.id = id;
      //alert("update liste membre"+JSON.stringify(res));
      console.log('info athlete '+JSON.stringify(res));
      this.nav.navigateRoot('addmembreclub/'+ JSON.stringify(res));
    });
  }

}
