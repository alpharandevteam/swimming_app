import { Component, OnInit } from '@angular/core';
import { Club, ClubServiceService } from '../../service/club-service.service';
import { Athletes, AthletesService } from './../../service/athletes.service';
import { ActivatedRoute } from '@angular/router';
import { NavController, LoadingController, ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-userhome',
  templateUrl: './userhome.page.html',
  styleUrls: ['./userhome.page.scss'],
})
export class UserhomePage implements OnInit {
  club;
  listeMembre;
  constructor(private route: ActivatedRoute, private nav: NavController, private clubService: ClubServiceService, private loadingController: LoadingController, private ath: AthletesService) { 
  }

  ngOnInit() {
    this.club = this.route.snapshot.params['id'];
    console.log(this.club);
  }
  goChrono() {
    this.nav.navigateForward('list-participant/' + this.club);
  }
  listemembreclub(){
    this.nav.navigateForward('listemembreclub/' + this.club);
  }
  listeperformance(){
    this.nav.navigateForward('performance/'+ this.club);
  }
  gestlogin(){
    this.nav.navigateForward('login');
  }
}
