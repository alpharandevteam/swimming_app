import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListParticipantPage } from './list-participant.page';

describe('ListParticipantPage', () => {
  let component: ListParticipantPage;
  let fixture: ComponentFixture<ListParticipantPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListParticipantPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListParticipantPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
