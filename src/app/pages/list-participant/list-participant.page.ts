import { Component, OnInit } from '@angular/core';
import { Athletes, AthletesService } from './../../service/athletes.service';
import { ActivatedRoute } from '@angular/router';
import { NavController, LoadingController } from '@ionic/angular';
import { TypeNatation, TypeNatationService } from './../../service/type-natation.service';
import { Competition, CompetitionService } from '../../service/competition.service';
@Component({
  selector: 'app-list-participant',
  templateUrl: './list-participant.page.html',
  styleUrls: ['./list-participant.page.scss'],
})
export class ListParticipantPage implements OnInit {
  club = null;
  resultat = null;
  selectedArray: any = [];
  listtypenatation =null;
  listCompetition = null;
  constructor(private route: ActivatedRoute, private nav: NavController,private athletesservice: AthletesService, private loadingController: LoadingController,private typenatationservice:TypeNatationService, private compet :CompetitionService) { }

  async ngOnInit() {
    const loading = await this.loadingController.create({
      message: 'Chargement..'
    });
    loading.present();
    this.club = this.route.snapshot.params['Nom'];
    this.typenatationservice.GetTypeNatations().subscribe( res => {
      this.listtypenatation = res;
    });
    this.compet.get_competition().subscribe( res => {
      this.listCompetition = res;
    });
    this.athletesservice.Getathletes_by_Club(this.club).subscribe(res => {
      this.resultat = res;
      if ( this.resultat.length !== 0 &&  this.resultat.length !== undefined ) {
        loading.dismiss();
      }
    });
  }
  async Go_to_chronos(item,IdTypeNatation,IdCompetition) {
    if(item !=null && item != '' && item != 0 && item != undefined && item.lenght !=0 && IdTypeNatation !=null && IdCompetition !=null  ){
    this.nav.navigateForward('stopwatch/' + JSON.stringify(item) + '/' + IdTypeNatation + '/' + IdCompetition);
  }else{ alert('Veuillez completer le formulaire')}
  }
  addCheckbox(event, iduser) {
    if (event.detail.checked) {
      this.selectedArray.push(iduser);
    } else {
      const index = this.removeCheckedFromArray(iduser);
      this.selectedArray.splice(index, 1);
    }
    console.log(this.selectedArray);
  }
  removeCheckedFromArray(checkbox) {
    this.selectedArray.findIndex((category) => {
   });
 }
 filter(IdTypeNatation){
   this.athletesservice.getbyid(IdTypeNatation).subscribe( res => {
    this.resultat = res;
    this.selectedArray=[];
    console.log(this.resultat);
  });
 }
}
