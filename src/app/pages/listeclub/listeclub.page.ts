import { Component, OnInit } from '@angular/core';
import { Club, ClubServiceService } from '../../service/club-service.service';
import { Athletes, AthletesService } from './../../service/athletes.service';
import { ActivatedRoute } from '@angular/router';
import { NavController, LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-listeclub',
  templateUrl: './listeclub.page.html',
  styleUrls: ['./listeclub.page.scss'],
})
export class ListeclubPage implements OnInit {
  club: Club[];
  constructor(private route: ActivatedRoute, private nav: NavController, private clubService: ClubServiceService, private loadingController: LoadingController, private athletesservice:AthletesService) { 

  }

  async ngOnInit() {
    this.club = this.route.snapshot.params['club'];
    const loading = await this.loadingController.create({
      message: 'Chargement..'
    });
    loading.present();
    this.clubService.getClub().subscribe(res => {
      console.log(res);
      this.club = res;
      console.log("liste club");
      console.log(this.club);
      if ( this.club.length !== 0 &&  this.club.length !== undefined ) {
        loading.dismiss();
      }
    });
  }
  async goto_list(Nom){
    const loading = await this.loadingController.create({
      message: 'Loading athletes..'
    });
    await loading.present();
    this.athletesservice.Getathletes_by_Club(Nom).subscribe(res => {
      loading.dismiss();
       console.log('frfdfd');
    console.log(JSON.stringify(res));
    this.nav.navigateForward('list-participant/' + JSON.stringify(res));
    });
  }
}
