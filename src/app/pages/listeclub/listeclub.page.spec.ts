import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeclubPage } from './listeclub.page';

describe('ListeclubPage', () => {
  let component: ListeclubPage;
  let fixture: ComponentFixture<ListeclubPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeclubPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeclubPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
