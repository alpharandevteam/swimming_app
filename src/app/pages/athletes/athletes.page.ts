import { Component, OnInit } from '@angular/core';
import { Athletes, AthletesService } from './../../service/athletes.service';
import { Club, ClubServiceService } from '../../service/club-service.service';
import { TypeNatation, TypeNatationService } from './../../service/type-natation.service';
import { NavController, LoadingController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-athletes',
  templateUrl: './athletes.page.html',
  styleUrls: ['./athletes.page.scss'],
})
export class AthletesPage implements OnInit {
  athletes: Athletes = {
    Nom: '',
    Poids: '',
    Age: '',
    Taille: '',
    IdClub: '',
    IdTypeNatation: ''
  };
  athletesId = null;
  listclub = null;
  listtypenatation = null;
  resultat = null;
  // tslint:disable-next-line:max-line-length
  constructor(private route: ActivatedRoute, private nav: NavController,private athletesservice: AthletesService, private loadingController: LoadingController,private typenatationservice:TypeNatationService,private clubservice:ClubServiceService) { }

  async ngOnInit() {
    this.athletesId = this.route.snapshot.params['id'];

    if(this.athletesId !="add"){
      this.resultat = JSON.parse(this.athletesId);
      this.listclub = await this.clubservice.getClub().subscribe(res => {
        this.listclub = res;
        console.log('List club');
        console.log(this.listclub);
      });
      this.listtypenatation = await this.typenatationservice.GetTypeNatations().subscribe(res => {
        this.listtypenatation = res;
        console.log(this.listtypenatation);
      });
    }else{
      this.listtypenatation = await this.typenatationservice.GetTypeNatations().subscribe(res => {
        this.listtypenatation = res;
        console.log(this.listtypenatation);
      });
      this.listclub = await this.clubservice.getClub().subscribe(res => {
        this.listclub = res;
        console.log('List club');
        console.log(this.listclub);
      });
    }
    console.log(this.resultat);
     
  }
  async loadathletes() {
    const loading = await this.loadingController.create({
      message: 'Loading type utilisateur..'
    });
    await loading.present();

    this.athletesservice.Getathletes(this.athletesId).subscribe(res => {
      loading.dismiss();
      this.athletes = res;
      console.log(this.athletes);
    });
  }
  async updatssaison(athletesId,athletesnom,athletespoids,athletesage,athletestaille,athletesclub,athletestypenat) {
    //alert(type_usersId);
    console.log(athletesId);
    
    const loading = await this.loadingController.create({
    message: 'Modification en cours..'
    });
    await loading.present();
    this.athletes.Nom = athletesnom;
    this.athletes.Poids = athletespoids;
    this.athletes.Age = athletesage;
    this.athletes.Taille = athletestaille;
    this.athletes.IdClub = athletesclub;
    this.athletes.IdTypeNatation = athletestypenat;
    //this.type_user.id=type_usersId;
    console.log(this.athletes);
    this.athletesservice.updateathletes(this.athletes, athletesId).then(() => {
    loading.dismiss();
    this.nav.navigateForward('list-athletes');
    });
    }
  async saveathletes() {
    const loading = await this.loadingController.create({
      message: 'Enregistrement en cours..'
    });
    await loading.present();
      this.athletesservice.addathletes(this.athletes).then(() => {
        loading.dismiss();
        this.nav.navigateForward('list-athletes');
      });
  }

}
