import { Component, OnInit } from '@angular/core';
import { Performance, PerformanceService } from './../../service/performance.service';
import { Athletes, AthletesService } from './../../service/athletes.service';
import { NavController, LoadingController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { Competition, CompetitionService } from '../../service/competition.service';
@Component({
  selector: 'app-performance',
  templateUrl: './performance.page.html',
  styleUrls: ['./performance.page.scss'],
})
export class PerformancePage implements OnInit {
  performance: Performance[];
  performances : Performance = {
    Chronos:'',
    IdAthletes: '',
    IdTypeNatation:'',
    typecompet:'',
    Date:new Date().getTime(),
  };
  club: any;
  resultat = null;
  listeNomSaison = null;
  constructor(private athletesservice: AthletesService,private performanceservice:PerformanceService,private loadingController:LoadingController, private nav:NavController, private route:ActivatedRoute,private compet:CompetitionService) { 

  }

  async ngOnInit() {
    await this.performanceservice.get_performance().subscribe(res => {
      this.performance = res;
      console.log(this.performance);
    });
    this.club = this.route.snapshot.params['id'];
    this.compet.get_competition().subscribe(res => {
      this.listeNomSaison = res;
    });
  }
  async deletefile(performanceId){
    const loading = await this.loadingController.create({
      message: 'Suppression en cours..',
    });
    await loading.present();
      this.performanceservice.removeperformance(performanceId).then(() => {
      loading.dismiss();
      this.nav.navigateForward('performance/' + this.club);
    });
  }
  filtrer(idcompet){
    this.performanceservice.getbynomcompet(idcompet).subscribe(res => {
      this.performance=res;
    });
  }
  filter_by_mot(mot){
    if(mot !=''){
    this.performanceservice.getbynom(mot).subscribe(res => {
      this.performance=res;
    });
  }else{
      this.performanceservice.get_performance().subscribe(res => {
      this.performance = res;
    });
  }
}
}
