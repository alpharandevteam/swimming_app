import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { User, AuthenticationServiceService } from './../../service/authentication-service.service';
import { Athletes,Club,Competition,Performance,Saison,TypeNatation,Type_Users, AllService } from './../../service/all.service';
import * as bcrypt from 'bcryptjs';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})

export class LoginPage implements OnInit {

  // tslint:disable-next-line:max-line-length
  constructor(private route: ActivatedRoute, private nav: NavController, private auths: AuthenticationServiceService, private load: LoadingController,private allService:AllService) { }

  ngOnInit() {
    this.allService.test();
  }
  reset_password() {
  }
  async gototypeuser() {
    this.nav.navigateForward('type-user');
  }
  async goto_list_type_user() {
    this.nav.navigateForward('list-type-user');
  }
  async login(form) {
    const loading = await this.load.create({
      message: 'loading...'
    });
    await loading.present();
    await this.auths.get_user_by_login(form.value.email).subscribe(async res1 => {
      if(res1.length == 0){
        loading.dismiss();
        alert('mail ou mot de passe incorrect');
      }
      console.log(res1);
      console.log(res1[0].club);
    if (res1.length !== 0) {
   const is_logged = await this.check_pass(res1[0].password, form.value.password);
    if (is_logged === true) {
      loading.dismiss();
      if (res1[0].typeUser === 'Admin') {
        this.nav.navigateForward('adminhome');
      } else if (res1[0].typeUser === 'User') {
        console.log(res1[0].club+'22');
        this.nav.navigateForward('userhome/' + res1[0].club);
      }
     } else {
      loading.dismiss();
       alert('mot de passe incorrect');
     }
  } else {
    loading.dismiss();
    alert('mail incorrect');
  }
    });
  }
  createAccount() {
    this.nav.navigateForward('users/add');
  }
  check_pass(cripted, pass) {
  return bcrypt.compare(pass, cripted);
  }
}
