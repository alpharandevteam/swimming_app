import { Component, OnInit } from '@angular/core';
import { User, AuthenticationServiceService } from '../../service/authentication-service.service';
import { ActivatedRoute } from '@angular/router';
import * as bcrypt from 'bcryptjs';
import { NavController, LoadingController } from '@ionic/angular';
import { Type_Users, TypeUsersService } from './../../service/type-users.service';
import { Club, ClubServiceService } from '../../service/club-service.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.page.html',
  styleUrls: ['./users.page.scss'],
})
export class UsersPage implements OnInit {
  password: string;
  users: User = {
    login: '',
    password: this.password,
    mail: '',
    typeUser: '',
    gestionForgotpwd: '',
    createdAt: new Date().getTime(),
    club: '',
  };
  listTypeUsers = null;
  listClub;
  // tslint:disable-next-line:max-line-length
  for_update;
  // tslint:disable-next-line:max-line-length
  constructor(private route: ActivatedRoute, private nav: NavController, private authenticationServiceService: AuthenticationServiceService, private loadingController: LoadingController, private typeuser: TypeUsersService, private clubservice: ClubServiceService) { 
    const getted = this.route.snapshot.paramMap.get('iduser');

    if (getted !== 'add' ) {
      console.log(JSON.parse(getted));

      this.for_update = JSON.parse(getted);
    }

  }

  async ngOnInit() {
    const loading = await this.loadingController.create({
      message: 'loading...'
    });
    await loading.present();
    this.listTypeUsers = await this.typeuser.GetType_User().subscribe(res => {
      this.listTypeUsers = res;
    });
    this.listClub = await this.clubservice.getClub().subscribe(res2 => {
      this.listClub = res2;
    });
    if (this.listTypeUsers.length !== 0) {
      loading.dismiss();
    }
  }

  async createUser(password) {
    const loading = await this.loadingController.create({
      message: 'loading...'
    });
    await loading.present();
    const salt = bcrypt.genSaltSync(10);
    const hash = bcrypt.hashSync('B4c0/\/', salt);
    bcrypt.hash(password, hash, (err, hashed) => {
      this.password = String(hashed);
      this.authenticationServiceService.adduser(this.users, this.password).then(() => {
        loading.dismiss();
        // this.nav.navigateForward('login');
        alert("Creation compte avec succes");
        this.nav.navigateForward('gestion-utilisateurs');
      });
    });
  }
  async  updade_user_data(for_update) {
    const loading = await this.loadingController.create({
      message: 'loading...'
    });
    await loading.present();
    for_update.date_creation = new Date().getTime();
    this.authenticationServiceService.updateUser(for_update, for_update.id).then(() => {
      loading.dismiss();
      this.nav.navigateForward('gestion-utilisateurs');
    });
  }
  async pagenatation() {
    this.nav.navigateForward('list-type-natation');
  }
  async pagesaison() {
    this.nav.navigateForward('list-saison');
  }
}

