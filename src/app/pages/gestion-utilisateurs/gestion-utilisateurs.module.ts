import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { GestionUtilisateursPage } from './gestion-utilisateurs.page';

const routes: Routes = [
  {
    path: '',
    component: GestionUtilisateursPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [GestionUtilisateursPage]
})
export class GestionUtilisateursPageModule {}
