import { Component, OnInit } from '@angular/core';
import { User, AuthenticationServiceService } from '../../service/authentication-service.service';
import { ActivatedRoute } from '@angular/router';
import { NavController, LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-gestion-utilisateurs',
  templateUrl: './gestion-utilisateurs.page.html',
  styleUrls: ['./gestion-utilisateurs.page.scss'],
})
export class GestionUtilisateursPage implements OnInit {
  login;
  users:User[];
  constructor(private activatedRoute: ActivatedRoute, private nav: NavController, private authenticationServiceService: AuthenticationServiceService, private loadingController: LoadingController) { 
    
  }

  ngOnInit() {    
    this.authenticationServiceService.getUsers().subscribe(res => {
      this.users = res;
      console.log(this.users);
    });
  }

  async update_user(iduser) {
    alert(iduser);
    const loading = await this.loadingController.create({
      message: 'Loading ...'
    });
    await loading.present();
    this.authenticationServiceService.get_user_by_id(iduser).subscribe(res => {
      loading.dismiss();
      res.id = iduser;
      console.log('frfdfd' + res);
      console.log(JSON.stringify(res));
      this.nav.navigateForward('users/' + JSON.stringify(res));      
    });
  }

  async delete_user(id) {
    const loading = await this.loadingController.create({
      message: 'Loading ...'
    });
    await loading.present();
    this.authenticationServiceService.removeUser(id).then(res => {
      loading.dismiss();
       alert('Succes');
    });
  }

  async add_user() {
    this.nav.navigateForward('users/add');
  }

}
