import { Component, OnInit } from '@angular/core';
import { Type_Users, TypeUsersService } from './../../service/type-users.service';
import { NavController, LoadingController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-list-type-user',
  templateUrl: './list-type-user.page.html',
  styleUrls: ['./list-type-user.page.scss'],
})
export class ListTypeUserPage implements OnInit {
  type_users: Type_Users[];
  type_user: Type_Users = {
    NomType: ''
  };
  type_usersId = null;
  constructor(private route: ActivatedRoute, private nav: NavController,private typeusersservice: TypeUsersService, private loadingController: LoadingController) { 
    this.typeusersservice.GetType_User().subscribe(res => {
      this.type_users = res;
      console.log(this.type_users);
    });
  }

  ngOnInit() {

  }
  async goto_insert_type_user(){
    let j= null;
    this.nav.navigateForward('type-user/' + j);
  }
  async goto_modifie_type_user(id){
    const loading = await this.loadingController.create({
      message: 'Loading type utilisateur..'
    });
    await loading.present();
    this.typeusersservice.GetType_Users(id).subscribe(res => {
      loading.dismiss();
      res.id = id;
       console.log('frfdfd' + res);
    console.log(JSON.stringify(res));
    this.nav.navigateForward('type-user/' + JSON.stringify(res));
    });
  }
  async delete(type_userId){
    const loading = await this.loadingController.create({
      message: 'Suppression en cours..',
      duration: 5000
    });
    await loading.present();
      this.typeusersservice.removeType_User(type_userId).then(() => {
      loading.dismiss();
      this.nav.navigateForward('list-type-user');
  });
  }
}
