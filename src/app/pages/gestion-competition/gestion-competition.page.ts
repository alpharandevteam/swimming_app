import { Component, OnInit } from '@angular/core';
import { Competition, CompetitionService } from '../../service/competition.service';
import { ActivatedRoute } from '@angular/router';
import { NavController, LoadingController } from '@ionic/angular';
@Component({
  selector: 'app-gestion-competition',
  templateUrl: './gestion-competition.page.html',
  styleUrls: ['./gestion-competition.page.scss'],
})
export class GestionCompetitionPage implements OnInit {
  competition:Competition[];
  constructor(private activatedRoute: ActivatedRoute, private nav: NavController, private competitionService: CompetitionService, private loadingController: LoadingController) { 
    
  }

  ngOnInit() {
    this.competitionService.get_competition().subscribe(res => {
      this.competition = res;
      console.log(this.competition);
    });
  }
  
  async delete_competition(id) {
    const loading = await this.loadingController.create({
      message: 'Loading ...'
    });
    await loading.present();
    this.competitionService.removeCompetition(id).then(res => {
      loading.dismiss();
       alert('Succes');
    });
  }
  async update_competition(id) {
    const loading = await this.loadingController.create({
      message: 'Loading Todo..'
    });
    await loading.present();
    this.competitionService.get_competition_by_id(id).subscribe(res => {
      loading.dismiss();
      res.id = id;
      console.log('info competition'+ res);
      console.log(JSON.stringify(res));
       this.nav.navigateForward('competition/' + JSON.stringify(res));
    });
  }
  async addCompetition(){
    this.nav.navigateForward('competition/add');
  }

}
