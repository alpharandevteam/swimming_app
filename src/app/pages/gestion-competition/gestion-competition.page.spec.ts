import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionCompetitionPage } from './gestion-competition.page';

describe('GestionCompetitionPage', () => {
  let component: GestionCompetitionPage;
  let fixture: ComponentFixture<GestionCompetitionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestionCompetitionPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionCompetitionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
