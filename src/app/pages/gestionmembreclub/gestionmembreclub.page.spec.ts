import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionmembreclubPage } from './gestionmembreclub.page';

describe('GestionmembreclubPage', () => {
  let component: GestionmembreclubPage;
  let fixture: ComponentFixture<GestionmembreclubPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestionmembreclubPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionmembreclubPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
