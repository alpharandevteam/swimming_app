import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { GestionmembreclubPage } from './gestionmembreclub.page';

const routes: Routes = [
  {
    path: '',
    component: GestionmembreclubPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [GestionmembreclubPage]
})
export class GestionmembreclubPageModule {}
