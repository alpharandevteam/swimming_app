import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController, LoadingController } from '@ionic/angular';
import { User, AuthenticationServiceService } from '../../service/authentication-service.service';

@Component({
  selector: 'app-modification-users',
  templateUrl: './modification-users.page.html',
  styleUrls: ['./modification-users.page.scss'],
})
export class ModificationUsersPage implements OnInit {
  users: User = {
    login: '',
    password: '',
    mail: '',
    typeUser: '',
    gestionForgotpwd: '',
    createdAt: new Date().getTime(),
    club: '',
  };
  idUser = null;
  infoUser:User[];

  constructor(private route: ActivatedRoute, private nav: NavController, private authenticationServiceService: AuthenticationServiceService, private loadingController: LoadingController) { }

  ngOnInit() {
    this.idUser = this.route.snapshot.params['id'];
    //alert(this.idUser);
    if (this.idUser) {
      this.loadUser();
    }
  }

  async loadUser() {
    // const loading = await this.loadingController.create({
    //   message: 'Chargement . . .'
    // });
    // await loading.present();
    // this.authenticationServiceService.getUser(this.idUser).subscribe(res => {
    //   loading.dismiss();
    //   this.infoUser = res;
    //   console.log(this.infoUser);
    // });
    this.authenticationServiceService.getUser(this.idUser).subscribe(res => {
      this.infoUser = res;
      console.log(this.infoUser);
    });
  }

}
