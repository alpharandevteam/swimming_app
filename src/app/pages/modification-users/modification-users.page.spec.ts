import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModificationUsersPage } from './modification-users.page';

describe('ModificationUsersPage', () => {
  let component: ModificationUsersPage;
  let fixture: ComponentFixture<ModificationUsersPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModificationUsersPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModificationUsersPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
