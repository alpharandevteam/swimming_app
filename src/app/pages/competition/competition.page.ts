import { Component, OnInit } from '@angular/core';
import { Competition, CompetitionService } from '../../service/competition.service';
import { ActivatedRoute } from '@angular/router';
import * as bcrypt from 'bcryptjs';
import { NavController, LoadingController } from '@ionic/angular';
import { Saison, SaisonService } from './../../service/saison.service';

@Component({
  selector: 'app-competition',
  templateUrl: './competition.page.html',
  styleUrls: ['./competition.page.scss'],
})
export class CompetitionPage implements OnInit {
  competition: Competition = {
    nomCompetition: '',
    nomSaison:'',
  };
  listeNomSaison;
  for_update: Competition;
  constructor(private route: ActivatedRoute, private nav: NavController, private competitionService: CompetitionService, private loadingController: LoadingController, private saisonService: SaisonService) { 
    const getted = this.route.snapshot.paramMap.get('idCompetition');
    if (getted !== 'add' ) {
      console.log(JSON.parse(getted));
      this.for_update = JSON.parse(getted);
      console.log("recupetation data competition à modifier");
      console.log(this.for_update);
    }
  }

  async ngOnInit() {
    this.listeNomSaison = await this.saisonService.Getsaisons().subscribe(res => {
      this.listeNomSaison = res;
     console.log("resultat saison");
      console.log(this.listeNomSaison);
      console.log("nom saison");
      console.log(this.listeNomSaison[0].Nom);
    });
  }

  async Save_competition(form) {
    const loading = await this.loadingController.create({
      message: 'loading...'
    });
    await loading.present();
    this.competition = form.value;
      this.competitionService.addcompetition(this.competition).then(() => {
        loading.dismiss();
        this.nav.navigateForward('gestion-competition');
      });
  }

  async  updade_competition_data(for_update) {
    const loading = await this.loadingController.create({
      message: 'loading...'
    });
    await loading.present();
    for_update.date_creation = new Date().getTime();
    this.competitionService.updateCompetition(for_update, for_update.id).then(() => {
      loading.dismiss();
      this.nav.navigateForward('gestion-competition');
    });
  }

}
