import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'inscription-users', loadChildren: './pages/inscription-users/inscription-users.module#InscriptionUsersPageModule' },
  { path: 'admin', loadChildren: './pages/admin/admin.module#AdminPageModule' },
  { path: 'club', loadChildren: './pages/club/club.module#ClubPageModule' },
  { path: 'competition/:idCompetition', loadChildren: './pages/competition/competition.module#CompetitionPageModule' },
  { path: 'type-natation/:id', loadChildren: './pages/type-natation/type-natation.module#TypeNatationPageModule' },
  { path: 'type-user/:id', loadChildren: './pages/type-user/type-user.module#TypeUserPageModule' },
  { path: 'saison/:id', loadChildren: './pages/saison/saison.module#SaisonPageModule' },
  { path: 'performance/:id', loadChildren: './pages/performance/performance.module#PerformancePageModule' },
  { path: 'athletes/:id', loadChildren: './pages/athletes/athletes.module#AthletesPageModule' },
  { path: 'users/:iduser', loadChildren: './pages/users/users.module#UsersPageModule' },
  { path: 'gestion-utilisateurs', loadChildren: './pages/gestion-utilisateurs/gestion-utilisateurs.module#GestionUtilisateursPageModule' },
  { path: 'list-type-user', loadChildren: './pages/list-type-user/list-type-user.module#ListTypeUserPageModule' },
  { path: 'modification-users/:id', loadChildren: './pages/modification-users/modification-users.module#ModificationUsersPageModule' },  
  { path: 'addclub/:categories', loadChildren: './pages/addclub/addclub.module#AddclubPageModule' },
  { path: 'stopwatch/:item/:IdTypeNatation/:idcompet', loadChildren: './pages/stopwatch/stopwatch.module#StopwatchPageModule' },
  { path: 'list-type-natation', loadChildren: './pages/list-type-natation/list-type-natation.module#ListTypeNatationPageModule' },  
  { path: 'list-athletes', loadChildren: './pages/list-athletes/list-athletes.module#ListAthletesPageModule' },
  { path: 'adminhome', loadChildren: './pages/adminhome/adminhome.module#AdminhomePageModule' },
  { path: 'list-type-natation', loadChildren: './pages/list-type-natation/list-type-natation.module#ListTypeNatationPageModule' },
  { path: 'list-saison', loadChildren: './pages/list-saison/list-saison.module#ListSaisonPageModule' },
  { path: 'gestion-competition', loadChildren: './pages/gestion-competition/gestion-competition.module#GestionCompetitionPageModule' },
  { path: 'userhome/:id', loadChildren: './pages/userhome/userhome.module#UserhomePageModule' },
  { path: 'listeclub/:club', loadChildren: './pages/listeclub/listeclub.module#ListeclubPageModule' },
  { path: 'listemembreclub/:club', loadChildren: './pages/listemembreclub/listemembreclub.module#ListemembreclubPageModule' },
  { path: 'list-participant/:Nom', loadChildren: './pages/list-participant/list-participant.module#ListParticipantPageModule' },
  { path: 'gestionmembreclub', loadChildren: './pages/gestionmembreclub/gestionmembreclub.module#GestionmembreclubPageModule' },
  { path: 'addmembreclub/:coachClub', loadChildren: './pages/addmembreclub/addmembreclub.module#AddmembreclubPageModule' },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
