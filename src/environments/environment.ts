// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAu-hiQMdPeBM_8tg7AXUN2IY0oQZ4EJjU',
    authDomain: 'swimmingapp-3c850.firebaseapp.com',
    databaseURL: 'https://swimmingapp-3c850.firebaseio.com',
    projectId: 'swimmingapp-3c850',
    storageBucket: 'swimmingapp-3c850.appspot.com',
    messagingSenderId: '466019480260'
  }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
